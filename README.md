Copyright &copy; 2020 Kirk Rader

# node-red-speech-server

Central speech processor for 100% cloud-free voice assistance in a home automation network.

![](./speech-server-flow.png)

## Overview

This flow is one part of a comprehensive solution providing voice assistant functionality to a home automation system without relying on any "cloud" based services such as those from Google, Amazon et al. It interoperates with services provided by [speech client][node-red-speech-client] and [intent automation][node-red-intent-automation] Node-RED flows, described separately.

In particular, this flow receives recorded audio streams via MQTT, attempts to convert the recorded speech to text and then the text to an intent and send the result as a corresponding MQTT message. Such intent messages can be acted upon by intent automation services connected to the same MQTT broker.

```mermaid
flowchart TD

  subgraph "Front End (one per room)"
    client[node-red-speech-client]
    microphone([Mic Array /<br/>Speaker])
  end

  subgraph "Back End (one per home)"
    server[node-red-speech-server]
    automation[node-red-intent-automation]
    broker[MQTT Broker]
  end

  subgraph "Local IoT Networks"
    devices[Various Devices]
  end

  client --> | 1. wait for wake phrase<br/>5. record utterance | client
  microphone --> | 2. wake phrase<br/>4. utterance | client
  client --> | 3. feedback sound<br/>6. feedback sound | microphone
  client --> | 7. recording | broker
  broker --> | 7. recording | server
  server --> | 8. Speech-to-Text<br/>9. Text-to-Intent | server
  server --> | 10. intent | broker
  broker --> | 10. intent | automation
  automation <--> | 11. monitoring and control | devices
  automation <--> | 12. dashboard | client

  classDef this text:black,fill:pink
  class server this
```

- See <https://gitlab.com/parasaurolophus/node-red-speech-client> for the implementation of [node-red-speech-client][]

- See <https://gitlab.com/parasaurolophus/node-red-intent-automation> for an example of [node-red-intent-automation][]

_Warning: The careful reader will note a slight discrepancy between the actual flow and the preceding diagram. Not shown in the diagram is that the flow is configured with two different broker configuration nodes. Incoming messages reference a broker running on the same machine as this flow using the `localhost` host name. Outgoing intent messages are sent to a broker referenced by a proper DNS based host name. The former specifies no security configuration while the latter requires both authentication and encryption. This is a slight optimization for my own local setup where I do have two separate Mosquitto instances, one which is unsecured but accessible only inside my LAN and a second one which acts as a proxy for remote access. If you have only one broker or use a reverse proxy like NGINX to provide similar load balancing then you can use a single broker configuration node for this flow's input and output. Where multiple brokers are used, you may have to add bridge specifications to their configuration files to automatically forward speech related messages between them to support certain scenarios such as processing speech inside the home that was captured outside the home._

## Dependencies

- [voice2json](http://voice2json.org/)
- [node-red-contrib-voice2json](https://github.com/johanneskropf/node-red-contrib-voice2json)
- [node-red-contrib-fan](https://github.com/johanneskropf/node-red-contrib-fan)

## Installation

1. Follow the instructions at <http://voice2json.org/install.html> to install _voice2json_ on the same machine as Node-RED

2. Follow the instructions at <https://github.com/johanneskropf/node-red-contrib-voice2json> to install _node-red-contrib-voice2json_

    - Be sure to download and unpack a speech profile into somewhere Node-RED can access as per the instructions

3. (Optional) Follow the [tutorial](https://github.com/johanneskropf/node-red-contrib-voice2json/wiki/Getting-started---Step-by-step-tutorial) to verify that you have everything set up correctly before importing the flow described below

    _Note: The tutorial and all the other voice2json documentation assume that you will be capturing speech and processing all on a single machine. The flow described below is designed to run on a central server and does not use any audio capture or playback devices. You will want to skip the tutorial if your Node-RED server does not have any suitable audio hardware._

4. Import [flows_pi4.json](./flows_pi4.json) into Node-RED

5. Edit the MQTT broker configuration nodes to match your setup

6. Edit the _voice2json_ configuration node to match your setup

    - Set the path to your downloaded speech profile
    - Edit _Sentences_ to meet your needs
    - Edit _Slots_ to meet your needs
    - Use the `inject` node named _train_ any time you alter the _voice2json_ configuration

## Usage

This flow is a back-end component with no dashboard or UI other than the Node-RED editor. It does send all `voice2json/intent/+` messages to the debug as an aid in monitoring and testing.

[node-red-speech-client]: https://gitlab.com/parasaurolophus/node-red-speech-client
[node-red-intent-automation]: https://gitlab.com/parasaurolophus/node-red-intent-automation
